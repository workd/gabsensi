<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';
    protected $fillable = ['deskripsi'];

    public function siswa()
    {
        return $this->hasMany('App\Models\Siswa');
    }

    public function presensi()
    {
        return $this->hasMany('App\Models\Presensi');
    }
}
