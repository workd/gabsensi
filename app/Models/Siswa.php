<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = ['nis','nama','alamat','jenis_kelamin','tempat_lahir',
        'tanggal_lahir','nama_ortu','hp_ortu','kelas_id','aktif'];

    protected $casts = [
        'aktif' => 'boolean'
    ];

    public static function boot()
    {
        parent::boot();

        self::creating(function($siswa) {
            $siswa->kode_qr = \Hash::make($siswa->nis.date('Y-m-d'));
        });
    }

    public function kelas()
    {
        return $this->belongsTo('App\Models\Kelas');
    }

    public function presensi()
    {
        return $this->hasMany('App\Models\Presensi');
    }

    public function getTanggalLahirAttribute($date)
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d', $date)->format('d-m-Y');
    }

    public function setTanggalLahirAttribute($date)
    {
        $this->attributes['tanggal_lahir'] = \Carbon\Carbon::createFromFormat('d-m-Y', $date)->format('Y-m-d');
    }

    public function scopeAktif($query)
    {
        return $query->where('aktif', 1);
    }
}
