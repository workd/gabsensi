<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SiswaFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nis' => 'required|unique:siswa,nis'.($this->method() == 'PUT' ? ','.$this->siswa : '') ,
            'nama' => 'required',
            'alamat' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'nama_ortu' => 'required',
            'hp_ortu' => 'required',
            'kelas_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nis.required' => 'No Induk harus diisi',
            'nis.unique' => 'No Induk sudah terdaftar',
            'nama.required' => 'Nama harus diisi',
            'alamat.required' => 'Alamat harus diisi',
            'jenis_kelamin.required' => 'Jenis Kelamin harus diisi',
            'tempat_lahir.required' => 'Tempat Lahir harus diisi',
            'tanggal_lahir.required' => 'Tanggal Lahir harus diisi',
            'nama_ortu.required' => 'Nama Orang Tua harus diisi',
            'hp_ortu.required' => 'No Telp Orang Tua harus diisi',
            'kelas_id.required' => 'Kelas harus diisi',
        ];
    }
}
