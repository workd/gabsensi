<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
use Yajra\Datatables\Html\Builder;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use \App\Http\Requests\SiswaFormRequest;

class SiswaController extends Controller
{
    public function index(Request $request, Builder $htmlBuilder)
    {
        if ($request->ajax()) {
            return Datatables::of(Siswa::with('kelas')->orderBy('id','desc'))
                ->editColumn('aktif', function($siswa) {
                    return $siswa->aktif ? 'Aktif' : 'Tidak Aktif';
                })
                ->addColumn('action', function($siswa) {
                    return view('vendor.datatables._action', [
                        'show_url' => route('siswa.show', $siswa->id),
                        'edit_url' => route('siswa.edit', $siswa->id),
                        'model' => $siswa,
                        'form_url' => route('siswa.destroy', $siswa->id),
                        'confirm_message' => 'Anda yakin ingin menghapus '.$siswa->nama.'?'
                    ]);
                })
                ->make(true);
        }

        $html = $htmlBuilder
            ->addColumn(['data'=>'id','name'=>'id','title'=>'ID','class'=>'all'])
            ->addColumn(['data'=>'nis','name'=>'nis','title'=>'NIS','class'=>'all'])
            ->addColumn(['data'=>'nama','name'=>'nama','title'=>'Nama Lengkap','class'=>'all'])
            ->addColumn(['data'=>'kelas.deskripsi','name'=>'kelas.deskripsi','title'=>'Kelas','class'=>'all'])
            ->addColumn(['data'=>'alamat','name'=>'alamat','title'=>'Alamat','class'=>'none'])
            ->addColumn(['data'=>'jenis_kelamin','name'=>'jenis_kelamin','title'=>'Jenis Kelamin','class'=>'all'])
            ->addColumn(['data'=>'tempat_lahir','name'=>'tempat_lahir','title'=>'Tempat Lahir','class'=>'none'])
            ->addColumn(['data'=>'tanggal_lahir','name'=>'tanggal_lahir','title'=>'Tanggal Lahir','class'=>'none'])
            ->addColumn(['data'=>'nama_ortu','name'=>'nama_ortu','title'=>'Nama Orang Tua','class'=>'none'])
            ->addColumn(['data'=>'hp_ortu','name'=>'hp_ortu','title'=>'No HP Orang Tua','class'=>'all'])
            ->addColumn(['data'=>'aktif','name'=>'aktif','title'=>'Aktif','class'=>'none'])
            ->addColumn(['data'=>'action','name'=>'action','title'=>'Action']);

        return view('siswa.index')->with(compact('html'));
    }

    public function create()
    {
        return view('siswa.create');
    }

    public function store(SiswaFormRequest $request)
    {
        $siswa = Siswa::create($request->all());

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Berhasil menyimpan '. $siswa->nama
        ]);

        return redirect()->route('siswa.index');
    }

    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);
        return view('siswa.edit')->with(compact('siswa'));
    }

    public function update(SiswaFormRequest $request, $id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->update($request->all());

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Berhasil menyimpan '. $siswa->nama
        ]);

        return redirect()->route('siswa.index');
    }

    public function show($id)
    {
        $siswa = Siswa::findOrFail($id);
        return view('siswa.show')->with(compact('siswa'));
    }

    public function destroy(Request $request, $id)
    {
        Siswa::destroy($id);

        $request->session()->flash('flash_notification', [
            'level' => 'success',
            'message' => 'Data berhasil dihapus'
        ]);

        return redirect()->route('siswa.index');
    }

    public function downloadQrcode($id)
    {
        $siswa = Siswa::findOrFail($id);
        $data = base64_decode('data:image/png;base64,'.\QrCode::format('png')->size(300)->generate($siswa->kode_qr));

        $im = imagecreatefromstring($data);
        if ($im !== false) {
            header('Content-Type: image/png');
            imagepng($im);
            imagedestroy($im);
        }
        else {
            echo 'An error occurred.';
        }
    }
}
