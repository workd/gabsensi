$(document).ready(function(){
    $(document.body).on('click', '.js-delete', function(e) {
        e.preventDefault();

        var $el = $(this);
        var text = $el.data('confirm') ? $el.data('confirm') : 'Anda yakin?';

        swal({
            title: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Batalkan",
            closeOnConfirm: true,
        }, function(isConfirm){
            if (isConfirm) {
                $el.next('form').trigger('submit');
            }
        });
    });

    if ($().select2) {
        $.fn.select2.defaults.set("theme", "bootstrap");
        $('.select2me').select2({
            placeholder: "Select",
            width: 'auto', 
            allowClear: true
        });

        $('.select2me').change(function () {
            $(this).closest('form').validate().element($(this));
        });
    }

    if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            orientation: "left",
            autoclose: true
        });
        //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
    }

    if (jQuery().html5_qrcode) {
        $('#reader').html5_qrcode(function(data){
            $.post('/scan-qrcode', {qrcode:data}, function(res) {
                
                if (res.status == 'success') {
                    var item = res.data;

                    var sound = new Howl({
                        src: '/audio/success.mp3'
                    }).play();

                    $('<tr>').html(
                        '<td>'+item.nis+'</td>'+
                        '<td>'+item.nama+'</td>'+
                        '<td>'+item.kelas+'</td>'+
                        '<td>'+item.jam+'</td>'
                    ).prependTo('#presensi-table');
                } else if (res.status == 'recorded') {
                    $("#presensi-debug").text('Kamu sudah absen hari ini');
                } else {
                    var sound = new Howl({
                        src: '/audio/failed.mp3'
                    }).play();

                    $("#presensi-debug").text('QRCode tidak terdaftar');
                }
            }, 'json');
        },
        function(error){
        }, function(videoError){
            $("#presensi-debug").text(videoError);
        }
        );
    }

    $(document.body).on("click", ".presensi-group > .btn", function(event){
        event.preventDefault();
        var $btn = $(this);
        var presensi = $btn.children("input").data("value");
        var id = $btn.children("input").data("presensi");
        $.ajax({
            url: '/presensi/'+id,
            method: 'PUT',
            data: {status:presensi, _token:window.gAbsensi.csrfToken},
            success: function(response) {
                if (response.status == 'success') {
                    $btn.addClass("btn-success active").siblings().removeClass("btn-success active");
                } else {
                    swal("Prosese gagal!", "Gagal mengupdate status presensi.", "error")
                }
            }
        });
    });

    // $('#tsel-summernote').summernote({
    //     height: 300,
    // });

    // $('.datepicker').datepicker({
    //     format: "yyyy-mm-dd",
    //     forceParse: false,
    //     autoclose: true,
    //     // startDate: '-80y',
    //     // endDate: '-15y'
    // });


    // var hash = document.location.hash;
    // if (hash) {
    //     $('.nav-tabs a[href="'+hash+'"]').tab('show');
    // } 

    // $('.nav-tabs a').on('shown', function (e) {
    //     window.location.hash = e.target.hash.replace("#", "#" + prefix);
    // });
});