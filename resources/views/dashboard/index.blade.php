@extends('layouts.app')

@section('content')

<div class="page-content">
    <div class="page-head">
        <div class="page-title">
            <h1>Aplikasi Presensi {{ Helper::pengaturan()->nama_sekolah }}</h1>
        </div>

        <div class="page-toolbar">
            @yield('page-toolbar')
        </div>
    </div>
    
    @include('layouts._flash')
    
    <div class="alert alert-success"><strong>Selamat datang,</strong> {{ Auth::user()->name }}</div>
    
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-book"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $counter['presensi'] }}">{{ $counter['presensi'] }}</span>
                    </div>
                    <div class="desc"> Presensi </div>
                </div>
                <a class="more" href="{{ route('presensi.index') }}"> Lihat
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat red">
                <div class="visual">
                    <i class="fa fa-users"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $counter['siswa'] }}"> {{ $counter['siswa'] }} </div>
                    <div class="desc"> Siswa </div>
                </div>
                <a class="more" href="{{ route('siswa.index') }}"> Lihat
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat green">
                <div class="visual">
                    <i class="fa fa-building-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $counter['kelas'] }}">{{ $counter['kelas'] }}</span>
                    </div>
                    <div class="desc"> Kelas </div>
                </div>
                <a class="more" href="{{ route('kelas.index') }}"> Lihat
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat purple">
                <div class="visual">
                    <i class="fa fa-calendar"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $counter['tahunAjaran'] }}">{{ $counter['tahunAjaran'] }}</div>
                    <div class="desc"> Tahun Ajaran </div>
                </div>
                <a class="more" href="{{ route('akademik.index') }}"> Lihat
                    <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    </div>
</div>

@endsection