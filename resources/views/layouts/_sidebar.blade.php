<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item {{ Route::is('dashboard') ? 'active' : '' }}">
                <a href="{{ url('/') }}" class="nav-link">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Presensi</h3>
            </li>
            <li class="nav-item {{ Route::is('scanqrcode.presensi') ? 'active' : '' }}">
                <a href="{{ route('scanqrcode.presensi') }}" class="nav-link " target="_blank">
                    <i class="icon-camera"></i>
                    <span class="title">Scan QRCode</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('presensi.*') ? 'active' : '' }}">
                <a href="{{ route('presensi.index') }}" class="nav-link ">
                    <i class="icon-list"></i>
                    <span class="title">Daftar Presensi</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('laporan.presensi') ? 'active' : '' }}">
                <a href="{{ route('laporan.presensi') }}" class="nav-link">
                    <i class="icon-doc"></i>
                    <span class="title">Laporan</span>
                </a>
            </li>
            <li class="heading">
                <h3 class="uppercase">Master</h3>
            </li>
            <li class="nav-item {{ Route::is('siswa.*') ? 'active' : '' }}">
                <a href="{{ route('siswa.index') }}" class="nav-link">
                    <i class="icon-users"></i>
                    <span class="title">Data Siswa</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('kelas.*') ? 'active' : '' }}">
                <a href="{{ route('kelas.index') }}" class="nav-link">
                    <i class="icon-graduation"></i>
                    <span class="title">Data Kelas</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('mutasi.*') ? 'active' : '' }}">
                <a href="{{ route('mutasi.index') }}" class="nav-link">
                    <i class="icon-directions"></i>
                    <span class="title">Mutasi Siswa</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('akademik.*') ? 'active' : '' }}">
                <a href="{{ route('akademik.index') }}" class="nav-link">
                    <i class="icon-paper-clip"></i>
                    <span class="title">Tahun Ajaran</span>
                </a>
            </li>
            <li class="nav-item {{ Route::is('pengaturan.*') ? 'active' : '' }}">
                <a href="{{ route('pengaturan.index') }}" class="nav-link">
                    <i class="icon-settings"></i>
                    <span class="title">Pengaturan</span>
                </a>
            </li>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->