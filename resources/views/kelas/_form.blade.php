<div class="form-body">
    @include('layouts._flash')

    <div class="form-group {{ $errors->has('deskripsi') ? 'has-error' : '' }}">
        {!! Form::label('deskripsi','Kelas',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            {!! Form::text('deskripsi', null, ['class'=>'form-control']) !!}
            {!! $errors->first('deskripsi', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn green">Submit</button>
            <a href="{{ URL::previous() }}" type="button" class="btn default">Cancel</a>
        </div>
    </div>
</div>  