@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="page-head">
        <div class="page-title">
            <h1>Data Siswa</h1>
        </div>
        <div class="page-toolbar">
            @yield('page-toolbar')
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Data Siswa</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Detil</span>
        </li>
    </ul>

    @include('layouts._flash')

    <div class="row">
        <div class="col-md-12">
            <div class="profile-sidebar">
                <div class="portlet light profile-sidebar-portlet bordered">
                    <div class="profile-userpic">
                        <img src="http://lorempixel.com/200/200/?94419" class="img-responsive" alt=""> 
                    </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> {{ $siswa->nama }} </div>
                        <div class="profile-usertitle-job">{{ $siswa->kelas->deskripsi }} </div>
                    </div>
                    <div class="profile-userbuttons">
                        <a href="{{ route('siswa.edit', $siswa) }}" type="button" class="btn btn-circle green btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                    </div>
                    <div class="profile-usermenu">

                    </div>
                </div>
                <div class="portlet light bordered">
                    <div class="row list-separated">
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <div class="uppercase profile-stat-title"> {{ $siswa->presensi()->where('status','H')->count() }} </div>
                            <div class="uppercase profile-stat-text"> Hadir </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <div class="uppercase profile-stat-title"> {{ $siswa->presensi()->where('status','I')->count() }} </div>
                            <div class="uppercase profile-stat-text"> Izin </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6">
                            <div class="uppercase profile-stat-title"> {{ $siswa->presensi()->where('status','A')->count() }} </div>
                            <div class="uppercase profile-stat-text"> Alpha </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Detil Siswa</span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab">Data Diri</a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_2" data-toggle="tab">QRCode</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- PERSONAL INFO TAB -->
                                    <div class="tab-pane active" id="tab_1_1">
                                        <table class="table table-light table-hover">
                                            <tr>
                                                <td><strong>No Induk</strong></td>
                                                <td>{{ $siswa->nis }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Nama Siswa</strong></td>
                                                <td>{{ $siswa->nama }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Alamat</strong></td>
                                                <td>{{ $siswa->alamat }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Jenis Kelamin</strong></td>
                                                <td>{{ $siswa->jenis_kelamin == 'L' ? 'Laki-Laki' : 'Perempuan' }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Tempat Lahir</strong></td>
                                                <td>{{ $siswa->tempat_lahir }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Tanggal Lahir</strong></td>
                                                <td>{{ $siswa->tempat_lahir }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Nama Orang Tua</strong></td>
                                                <td>{{ $siswa->nama_ortu }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>HP Orang Tua</strong></td>
                                                <td>{{ $siswa->hp_ortu }}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Kelas</strong></td>
                                                <td>{{ $siswa->kelas->deskripsi }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="tab_1_2">
                                        <div class="center-block">
                                            <img class="img-responsive center-block" src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate($siswa->kode_qr)) !!} ">
                                            <p class="text-center">
                                                <a href="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(300)->generate($siswa->kode_qr)) !!} " download="qrcode.png" class="btn btn-primary text-center"><i class="fa fa-download"></i> Download</a>

                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PROFILE CONTENT -->
        </div>
    </div>
</div>
@endsection