<div class="form-body">
    @include('layouts._flash')

    <div class="form-group {{ $errors->has('nis') ? 'has-error' : '' }}">
        {!! Form::label('nis','No Induk',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            {!! Form::text('nis', null, ['class'=>'form-control']) !!}
            {!! $errors->first('nis', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
        {!! Form::label('nama','Nama Siswa',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-6">
            {!! Form::text('nama', null, ['class'=>'form-control']) !!}
            {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('alamat') ? 'has-error' : '' }}">
        {!! Form::label('alamat','Alamat',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-6">
            {!! Form::textarea('alamat', null, ['class'=>'form-control','rows'=>'5']) !!}
            {!! $errors->first('alamat', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('jenis_kelamin') ? 'has-error' : '' }}"">
        {!! Form::label('jenis_kelamin','Jenis Kelamin',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            {!! Form::select('jenis_kelamin', [''=>'-- Pilih Jenis Kelamin--', 'L'=>'Laki-Laki','P'=>'Perempuan'], null, ['class'=>'form-control']) !!}
            {!! $errors->first('jenis_kelamin', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('tempat_lahir') ? 'has-error' : '' }}">
        {!! Form::label('tempat_lahir','Tempat Lahir',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            {!! Form::text('tempat_lahir', null, ['class'=>'form-control']) !!}
            {!! $errors->first('tempat_lahir', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('tanggal_lahir') ? 'has-error' : '' }}">
        {!! Form::label('tanggal_lahir','Tanggal Lahir',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            <div class="input-group date date-picker" data-date-format="dd-mm-yyyy">
                {!! Form::text('tanggal_lahir', null, ['class'=>'form-control','readonly'=>'readonly']) !!}
                <span class="input-group-btn">
                    <button class="btn default" type="button">
                        <i class="fa fa-calendar"></i>
                    </button>
                </span>
            </div>
            {!! $errors->first('tanggal_lahir', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('nama_ortu') ? 'has-error' : '' }}">
        {!! Form::label('nama_ortu','Nama Orang Tua',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-6">
            {!! Form::text('nama_ortu', null, ['class'=>'form-control']) !!}
            {!! $errors->first('nama_ortu', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('hp_ortu') ? 'has-error' : '' }}">
        {!! Form::label('hp_ortu','No Telp Orang Tua',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            {!! Form::text('hp_ortu', null, ['class'=>'form-control']) !!}
            {!! $errors->first('hp_ortu', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group {{ $errors->has('kelas_id') ? 'has-error' : '' }}"">
        {!! Form::label('kelas_id','Jenis Kelamin',['class'=>'control-label col-md-3']) !!}
        <div class="col-md-4">
            {!! Form::select('kelas_id', [''=>''] + App\Models\Kelas::pluck('deskripsi','id')->toArray(), null, ['class'=>'form-control select2me']) !!}
            {!! $errors->first('kelas_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn green">Submit</button>
            <a href="{{ URL::previous() }}" type="button" class="btn default">Cancel</a>
        </div>
    </div>
</div>  