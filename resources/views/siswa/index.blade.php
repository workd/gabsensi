@extends('layouts.app')

@section('page-actions')
<div class="page-actions">
    <div class="btn-group">
        <button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <span class="hidden-sm hidden-xs">Tambah&nbsp;</span>
            <i class="fa fa-angle-down"></i>
        </button>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="{{ route('siswa.create') }}">
                    <i class="icon-plus"></i> Tambah 
                </a>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="icon-cloud-upload"></i> Import 
                </a>
            </li>
        </ul>
    </div>
</div>
@endsection

@section('content')
<div class="page-content">
    <div class="page-head">
        <div class="page-title">
            <h1>Data Siswa</h1>
        </div>
        <div class="page-toolbar">
            @yield('page-toolbar')
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Data Siswa</span>
        </li>
    </ul>

    @include('layouts._flash')

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-users font-green"></i>
                        <span class="caption-subject bold uppercase">Siswa</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        {!! $html->table(['class'=>'table table-striped table-bordered table-hover dt-responsive','width'=>'100%']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
{!! $html->scripts() !!}
@endsection