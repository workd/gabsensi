@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="page-head">
        <div class="page-title">
            <h1>Data Siswa</h1>
        </div>
        <div class="page-toolbar">
            @yield('page-toolbar')
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('siswa.index') }}">Data Siswa</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Edit</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-users font-green"></i>
                        <span class="caption-subject bold uppercase">Edit Siswa</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    {!! Form::model($siswa->toArray(), ['url'=>route('siswa.update', $siswa->id), 'method'=>'PUT', 'class'=>'form-horizontal']) !!}
                        @include('siswa._form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection