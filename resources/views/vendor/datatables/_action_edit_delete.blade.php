<a class="btn btn-xs btn-info" href="{{ $edit_url }}">Edit</a>
<a class="btn btn-xs btn-danger js-delete" href="{{ $form_url }}">Delete</a>
{!! Form::model($model, ['url'=>$form_url, 'method'=>'delete', 'style'=>'display:none']) !!}
{!! Form::close() !!}