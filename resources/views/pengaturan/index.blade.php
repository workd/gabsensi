@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="page-head">
        <div class="page-title">
            <h1>Pengaturan</h1>
        </div>
        <div class="page-toolbar">
            @yield('page-toolbar')
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Pengaturan</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-users font-green"></i>
                        <span class="caption-subject bold uppercase">Pengaturan</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['url'=>route('pengaturan.update'), 'method'=>'POST', 'class'=>'form-horizontal']) !!}
                        <div class="form-body">
                            @include('layouts._flash')

                            <div class="form-group {{ $errors->has('nama_aplikasi') ? 'has-error' : '' }}">
                                {!! Form::label('nama_aplikasi','Nama Aplikasi',['class'=>'control-label col-md-3']) !!}
                                <div class="col-md-6">
                                    {!! Form::text('nama_aplikasi', $pengaturan->nama_aplikasi, ['class'=>'form-control']) !!}
                                    {!! $errors->first('nama_aplikasi', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('nama_sekolah') ? 'has-error' : '' }}">
                                {!! Form::label('nama_sekolah','Nama Sekolah',['class'=>'control-label col-md-3']) !!}
                                <div class="col-md-6">
                                    {!! Form::text('nama_sekolah', $pengaturan->nama_sekolah, ['class'=>'form-control']) !!}
                                    {!! $errors->first('nama_sekolah', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                    <a href="{{ URL::previous() }}" type="button" class="btn default">Cancel</a>
                                </div>
                            </div>
                        </div>  
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection