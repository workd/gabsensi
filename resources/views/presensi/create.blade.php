@extends('layouts.app')

@section('content')
<div class="page-content">
    <div class="page-head">
        <div class="page-title">
            <h1>Input Presensi</h1>
        </div>
        <div class="page-toolbar">
            @yield('page-toolbar')
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <a href="{{ url('/') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('siswa.index') }}">Data Presensi</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span class="active">Tambah</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light portlet-fit portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-users font-green"></i>
                        <span class="caption-subject bold uppercase">Tambah Presensi</span>
                    </div>
                    <div class="tools"> </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['url'=>route('presensi.store'), 'method'=>'POST', 'class'=>'form-horizontal']) !!}
                        @include('presensi._form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection