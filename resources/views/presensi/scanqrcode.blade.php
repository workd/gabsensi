@extends('layouts.presensi')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-green">
                        <i class="icon-settings font-green"></i>
                        <span class="caption-subject bold uppercase">Presensi</span>
                    </div>
                    <div class="tools"> 
                        <a href="" class="fullscreen"> </a>
                    </div>
                </div>
                <div class="portlet-body" style="min-height:100%">
                    <div class="row">
                        <div class="col-md-4">
                            <div id="reader" class="well" style="width:100%;height:330px" ></div>
                            <p id="presensi-debug" class="text-center text-danger"></p>
                        </div>
                        <div class="col-md-8 fill">
                            <h1 id="timeLabel">12:00:00</h1>
                            <p> {{ date('l, d F Y') }}</p>
                            <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="presensi-table">
                                <thead>
                                    <tr>
                                        <th>NIS</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th>Jam Masuk</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($presensi as $absen)
                                        <tr>
                                            <td>{{ $absen->siswa->nis }}</td>
                                            <td>{{ $absen->siswa->nama }}</td>
                                            <td>{{ $absen->kelas->deskripsi }}</td>
                                            <td>{{ $absen->jam_masuk }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .fill { 
    min-height: 100%;
    height: 100%;
}
    </style>
@endsection

@section('scripts')
<script>
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('timeLabel').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
$(function(){
    startTime();
})
</script>
@endsection