<?php

use Illuminate\Database\Seeder;

class UsersTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\User::create([
            'username' => 'admin',
            'name' => 'Admin',
            'password' => \Hash::make('kilerjo'),
            'role' => 'admin'
        ]);
    }
}
