<?php

use Illuminate\Database\Seeder;
use App\Models\Siswa;
use Faker\Factory as Faker;

class SiswaTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,100) as $index) {
            Siswa::create([
                'nis' => $faker->unique()->numberBetween(5110001,512999),
                'nama' => $faker->name,
                'alamat' => $faker->address,
                'jenis_kelamin' => ['L','P'][rand(0,1)],
                'tempat_lahir' => $faker->state,
                'tanggal_lahir' => $faker->dateTimeThisCentury->format('d-m-Y'),
                'nama_ortu' => $faker->name,
                'hp_ortu' => $faker->phoneNumber,
                'kelas_id' => $faker->numberBetween(1,6),
                'aktif' => true,
                'foto' => $faker->imageUrl(200,300),
                'kode_qr' => $faker->sha256
            ]);
        }
        
    }
}
