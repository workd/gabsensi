<?php

use Illuminate\Database\Seeder;

class KelasTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Kelas::create(['deskripsi'=>'X A TKJ']);
        App\Models\Kelas::create(['deskripsi'=>'X B TKJ']);
        App\Models\Kelas::create(['deskripsi'=>'XI A TKJ']);
        App\Models\Kelas::create(['deskripsi'=>'XI B TKJ']);
        App\Models\Kelas::create(['deskripsi'=>'XII A TKJ']);
        App\Models\Kelas::create(['deskripsi'=>'XII B TKJ']);
    }
}
