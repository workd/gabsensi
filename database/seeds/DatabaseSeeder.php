<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeders::class);
        $this->call(AkademikTableSeeders::class);
        $this->call(KelasTableSeeders::class);
        $this->call(SiswaTableSeeders::class);
        $this->call(PresensiTableSeeders::class);
        $this->call(PengaturanTableSeeders::class);
    }
}
