<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMutasiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutasi', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('siswa_id')->unsigned();
            $table->date('tanggal');
            $table->integer('kelas_asal')->unsigned();
            $table->integer('kelas_tujuan')->unsigned();
            $table->integer('akademik_id')->unsigned();
            $table->enum('status_mutasi',['Keluar','Lulus','Naik Kelas']);
            $table->string('keterangan')->nullable();
            $table->timestamps();

            $table->foreign('siswa_id')->references('id')->on('siswa')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('kelas_asal')->references('id')->on('kelas')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('kelas_tujuan')->references('id')->on('kelas')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutasi');
    }
}
