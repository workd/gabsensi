<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswaTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nis')->unique();
            $table->string('nama');
            $table->text('alamat');
            $table->enum('jenis_kelamin', ['L','P']);
            $table->string('tempat_lahir');
            $table->date('tanggal_lahir');
            $table->string('nama_ortu')->nullable();
            $table->string('hp_ortu')->nullable();
            $table->integer('kelas_id')->unsigned();
            $table->boolean('aktif')->default(true);
            $table->string('foto')->nullable();
            $table->string('kode_qr');
            $table->timestamps();

            $table->foreign('kelas_id')->references('id')->on('kelas')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
