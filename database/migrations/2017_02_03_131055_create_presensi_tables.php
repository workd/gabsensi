<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresensiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presensi', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->integer('siswa_id')->unsigned();
            $table->integer('kelas_id')->unsigned();
            $table->integer('akademik_id')->unsigned();
            $table->enum('status', ['I','S','A','H']);
            $table->time('jam_masuk');
            $table->time('jam_pulang');
            $table->timestamps();

            $table->foreign('siswa_id')->references('id')->on('siswa')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('kelas_id')->references('id')->on('kelas')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('akademik_id')->references('id')->on('akademik')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unique( array('tanggal','siswa_id') );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presensi');
    }
}
