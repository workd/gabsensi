<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');

Route::group(['middleware'=>'auth'], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::resource('siswa', 'SiswaController');
    Route::get('download-qrcode/{id}', 'SiswaController@downloadQrcode')->name('downloadqrcode.siswa');
    Route::resource('kelas', 'KelasController');
    Route::resource('presensi', 'PresensiController', ['only' => ['index', 'create', 'store','update']]);
    Route::resource('mutasi', 'MutasiController');
    Route::resource('akademik', 'AkademikController');
    Route::get('/pengaturan', 'PengaturanController@index')->name('pengaturan.index');
    Route::post('/pengaturan', 'PengaturanController@update')->name('pengaturan.update');

    Route::get('laporan', 'PresensiController@laporan')->name('laporan.presensi');
    Route::any('scan-qrcode', 'PresensiController@scanQrcode')->name('scanqrcode.presensi');
});
